package com.service.lowestattendance;

import com.util.protoobjects.AttendanceOuterClass;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;

import java.io.IOException;
import java.util.HashMap;

public class AttendanceLookupBuilder {

    public HashMap<Integer, Integer> get(String Path, int attendanceNoOfDays) {
        HashMap<Integer, Integer> attendanceMap = new HashMap<>();
        Configuration conf = new Configuration();
        SequenceFile.Reader reader = null;
        try {
            Path inFile = new Path(Path);
            reader = new SequenceFile.Reader(conf, SequenceFile.Reader.file(inFile),
                    SequenceFile.Reader.bufferSize(4096));
            buildLookup(attendanceNoOfDays, attendanceMap, reader);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                    IOUtils.closeStream(reader);
            }
        }
        return attendanceMap;
    }

    private void buildLookup(int attendanceNoOfDays, HashMap<Integer, Integer> attendanceMap,
                             SequenceFile.Reader reader) throws IOException {
        IntWritable key = new IntWritable(); //set key as IntWritable
        ImmutableBytesWritable value = new ImmutableBytesWritable(); //set value ImmutableBytesWritable
        while (reader.next(key, value)) {
            AttendanceOuterClass.Attendance.Builder e =
                    AttendanceOuterClass.Attendance.newBuilder().mergeFrom(value.get());
            int employee_id = Integer.parseInt(String.valueOf(e.getEmployeeId()));
            int noOfDaysPresent = 0;
            for (int i = 0; i < attendanceNoOfDays; i++) {
                AttendanceOuterClass.DatePresence.Builder datePresence =
                        e.getDatesPresentBuilder(i);
                if (datePresence.getIsPresent()) {
                    noOfDaysPresent++;
                }
            }
            //set attendanceMap with employee_id and no of days employee was present
            attendanceMap.put(employee_id, noOfDaysPresent);
        }
    }
}

package com.service.loadprototohbase;

import com.util.HBaseTableCreator;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.HFileOutputFormat2;
import org.apache.hadoop.hbase.mapreduce.LoadIncrementalHFiles;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;

import java.io.IOException;
import java.util.ArrayList;

public class BuildingHbaseDriver extends Configured implements Tool {
    private static final String BUILDING_PROTOJOB_NAME = "Load Building Proto To Hbase";
    private static final String BUILDING_SEQ_FILE = "hdfs://localhost:8020/ProtoFiles/building.seq";
    private static final String BUILDING_TABLE_NAME = "building";
    private static final String BUILDING_PROTO_HBASE_PATH = "hdfs://localhost:8020/BuildingProtoHbase";


    private Job createSubmittableJob(Configuration conf, Path outPath, String input) {
        Job job = null;
        try {
            job = new Job();
            job.setJobName(BUILDING_PROTOJOB_NAME); //set job name
            job.setJarByClass(BuildingHbaseMapper.class);
            job.setMapperClass(BuildingHbaseMapper.class); //set mapper class
            job.setInputFormatClass(SequenceFileInputFormat.class); //set input format class
            job.setMapOutputKeyClass(ImmutableBytesWritable.class); //set map output key class
            job.setMapOutputValueClass(Put.class); //set map output value class
            FileInputFormat.setInputPaths(job, new Path(input)); //set input path
            FileOutputFormat.setOutputPath(job, outPath);  //set output path

            ArrayList<String> buildingColFamList = new ArrayList<>();
            buildingColFamList.add("building_details");

            configureLoad(conf, job, buildingColFamList);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return job;
    }

    private void configureLoad(Configuration conf, Job job, ArrayList<String> buildingColFamList) throws IOException {
        Connection connection = ConnectionFactory.createConnection(conf);
        // creating hbase table
        new HBaseTableCreator(connection, BUILDING_TABLE_NAME,
                buildingColFamList).CreateTable();
        TableName tableName = TableName.valueOf(BUILDING_TABLE_NAME);
        Table table = connection.getTable(tableName);
        RegionLocator regionLocator = connection.getRegionLocator(tableName);
        //calling incremental load
        HFileOutputFormat2.configureIncrementalLoad(job, table, regionLocator);
    }

    public int run(final String[] args) {
        Configuration conf = HBaseConfiguration.create(getConf());
        setConf(conf);
        Path outPath = new Path(BUILDING_PROTO_HBASE_PATH);
        String input = BUILDING_SEQ_FILE;
        Job job = createSubmittableJob(conf, outPath, input);
        boolean success = false;
        try {
            success = job.waitForCompletion(true);
            doBulkLoad(BUILDING_TABLE_NAME, outPath);
        } catch (IOException | InterruptedException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return success ? 0 : 1;
    }

    private void doBulkLoad(String tableNameString, Path tmpPath) throws IOException {
        LoadIncrementalHFiles loader = new LoadIncrementalHFiles(getConf());
        Connection connection = ConnectionFactory.createConnection(getConf());
        Admin admin = connection.getAdmin();
        TableName tableName = TableName.valueOf(tableNameString);
        Table table = connection.getTable(tableName);
        RegionLocator regionLocator = connection.getRegionLocator(tableName);
        //bulk load hbase table
        loader.doBulkLoad(tmpPath, admin, table, regionLocator);
    }

}
